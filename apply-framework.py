import gitlab
import requests

# PAT for querying all projects in a group: https://gitlab.com/-/profile/personal_access_tokens
# Note: Be sure to give this token api access!
PRIVATE_TOKEN = {YOUR_PAT}
# Generate token from creating a Group application: https://docs.gitlab.com/ee/integration/oauth_provider.html#group-owned-applications
# Then follow steps listed here: https://docs.gitlab.com/ee/api/oauth2.html#authorization-code-flow
# Note: Be sure to give application api, read/write repo, and sudo access - but probably just api access.
AUTH_TOKEN = {YOUR_AUTH_TOKEN}

# Apply framework to each project
def apply_framework(framework_id, project_gid):
    apply_framework_mutation = (
    """
        mutation {
        projectSetComplianceFramework(input: {projectId: 
        """
        + project_gid
        +
        """
        , complianceFrameworkId:
        """
        + framework_id
        +
        """
        }) {
            clientMutationId
            errors
            project {
                name
            }
        }
    }
    """)

    print(apply_framework_mutation)

    headers = {"Authorization": "Bearer " + AUTH_TOKEN, "Content-Type": "application/json"}
    r = requests.post("https://gitlab.com/api/graphql", json={"query":apply_framework_mutation}, headers=headers)
    result = r.json()
    print(result)

# Apply framework to all projects in given list
def apply_framework_to_projects(framework, projects_list):
    for project in projects_list:
        project_gid = ("\"gid://gitlab/Project/" + str(project.id) + "\"")
        apply_framework(framework, project_gid)

# Get all projects in a specified group
def get_projects(gl, group_id):
    group = gl.groups.get(group_id)
    return group.projects.list(all=True, include_subgroups=True)


def authenticate():
    gl = gitlab.Gitlab('https://gitlab.com', private_token=PRIVATE_TOKEN)
    print("Begin authenticating...")
    gl.auth()
    print("Authentication successful!")
    return gl


def main():
    gl = authenticate()

    # Get projects in group
    # Insert your group ID for the group you want to apply a compliance framework to, listed on your group page under the name
    group_id = {YOUR_GROUP_ID}
    projects_list = get_projects(gl, group_id)

    # Define your framework
    framework = "\"gid://gitlab/ComplianceManagement::Framework/{YOUR_FRAMEWORK_ID}\""

    apply_framework_to_projects(framework, projects_list)

main()
